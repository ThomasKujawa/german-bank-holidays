<?php

declare(strict_types = 1);

namespace GermanBankHoliday\Model;

use DateTime;
use DateTimeInterface;
use Exception;
use OutOfBoundsException;

class BankHoliday
{
    public const THRESHOLD_YEAR = 2022;
    protected const FIXED_DATES = [
        [
            'day'   => '01',
            'month' => '01',
            'key'   => 'neujahrstag',
        ],
        [
            'day'   => '06',
            'month' => '01',
            'key'   => 'heilige-drei-koenige',
        ],
        [
            'day'   => '08',
            'month' => '03',
            'key'   => 'frauentag',
        ],
        [
            'day'   => '01',
            'month' => '05',
            'key'   => 'tag-der-arbeit',
        ],
        [
            'day'   => '08',
            'month' => '08',
            'key'   => 'Augsburger Hohes Friedensfest',
        ],
        [
            'day'   => '15',
            'month' => '08',
            'key'   => 'mariae-himmelfahrt',
        ],
        [
            'day'   => '20',
            'month' => '09',
            'key'   => 'internationaler-kindertag',
        ],
        [
            'day'   => '31',
            'month' => '10',
            'key'   => 'reformationstag',
        ],
        [
            'day'   => '01',
            'month' => '11',
            'key'   => 'allerheiligen',
        ],
        [
            'day'   => '25',
            'month' => '12',
            'key'   => 'erster-weihnachtstag',
        ],
        [
            'day'   => '26',
            'month' => '12',
            'key'   => 'zweiter-weihnachtstag',
        ],
    ];

    /**
     * @param DateTimeInterface $date
     *
     * @return bool
     * @throws Exception
     */
    public function isHoliday(DateTimeInterface $date): bool
    {
        if ((int)$date->format('Y') < static::THRESHOLD_YEAR) {
            throw new OutOfBoundsException(
                sprintf('No bank holiday information before year %s', static::THRESHOLD_YEAR)
            );
        }

        $origin = new DateTime($date->format('Y-m-d'));

        foreach (static::FIXED_DATES as $holiday) {
            $knownHolidayString = sprintf(
                '%s-%s-%s',
                $date->format('Y'),
                $holiday['month'],
                $holiday['day']
            );
            $knownHoliday       = new DateTime($knownHolidayString);

            if ((int)($origin->diff($knownHoliday))->format('%a') === 0) {
                return true;
            }
        }

        return false;
    }
}

