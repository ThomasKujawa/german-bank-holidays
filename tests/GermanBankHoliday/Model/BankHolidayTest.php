<?php

namespace GermanBankHoliday\Model;

use DateTime;
use Exception;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;

class BankHolidayTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testYearOutOfBoundsException(): void
    {
        $this->expectException(OutOfBoundsException::class);

        $model = new BankHoliday();

        $dateString = sprintf(
            '%s-05-01',
            BankHoliday::THRESHOLD_YEAR - 1
        );
        $model->isHoliday(new DateTime($dateString));
    }
}

